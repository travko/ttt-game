package bot;

import action.reader.MoveDataReader;
import model.ModelProxy;
import org.apache.log4j.Logger;
import util.ClassName;
import util.Tuple;

import java.io.PrintStream;

public abstract class Bot {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private final PrintStream outputStream;
    public final ModelProxy model;

    public Bot(PrintStream output, ModelProxy modelProxy) {
        logger.debug("crating new bot");
        outputStream = output;
        model = modelProxy;
        logger.debug("setting model proxy callback");
        model.setMoveEventCallback(new ModelProxy.ModelMoveEvent() {
            @Override
            public void modelMoveEvent(int row, int column) {
                moveEvent(row, column);
            }
        });
    }

    private void commitMove(Tuple<Integer> nextMove) {
        logger.debug(String.format("committing move %s to model", nextMove));
        model.doMoveNoHandle(nextMove.getValue(0), nextMove.getValue(1));
    }

    protected void doMove(Tuple<Integer> nextMove) {
        logger.debug("doing bot move");
        commitMove(nextMove);
        logger.debug("sending move to the user");
        sendMove(MoveDataReader.EncodeMove(nextMove));
    }

    protected void sendMove(String line) {
        logger.debug("sending string command: " + line);
        outputStream.println(line);
    }

    protected abstract void moveEvent(int row, int column);

}
