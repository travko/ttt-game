package bot;

import model.field.IField;
import model.ModelProxy;
import org.apache.log4j.Logger;
import util.ClassName;
import util.Tuple;

import java.io.PrintStream;
import java.util.Random;

public class RandomBot extends Bot {

    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    public RandomBot(PrintStream output, ModelProxy modelProxy) {
        super(output, modelProxy);
    }

    @Override
    protected void moveEvent(int row, int column) {
        logger.debug("calling move event");
        doMove(getNextMove(model.getField()));
    }

    private Tuple<Integer> getNextMove(IField field) {
        logger.debug("generating next move");
        int row, column;
        Random random = new Random();

        do {
            row = random.nextInt(field.getFieldSize());
            column = random.nextInt(field.getFieldSize());
        } while (!field.isCellEmpty(row, column));

        logger.debug(String.format("next move position [%d, %d]", row, column));
        return new Tuple<>(new Integer[] {row, column});
    }

}
