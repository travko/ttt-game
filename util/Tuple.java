package util;

public class Tuple<T> {
    private final T[] _values;

    // T[] instead of T... for safety
    public Tuple(T[] values) {
        if (values == null || values.length == 0) {
            throw new IllegalArgumentException();
        }
        this._values = values;
    }

    public T getValue(final int index) {
        if (index < _values.length) {
            return _values[index];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public int length() {
        return this._values.length;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append('(');
        for (int i = 0; i < length(); i++) {
            if (i > 0) {
                builder.append(", ");
            }
            builder.append(getValue(i));
        }
        builder.append(')');
        return builder.toString();
    }
}
