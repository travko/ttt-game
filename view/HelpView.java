package view;

import org.apache.log4j.Logger;
import util.ClassName;

public class HelpView extends ConsoleView {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String NEW_LINE = "\r\n";
    private static final String HELP_MESSAGE =
            "Available command line switches list:" + NEW_LINE +
                    NEW_LINE +
                    "\t--mode   select a game mode from the list below:" + NEW_LINE +
                    "\t         * Bot    - enable game with Bot (in develop)" + NEW_LINE +
                    "\t         * client - enable client mode, also you should specify port and server ip address" + NEW_LINE +
                    "\t         * hotseat - enable hot seat mode" + NEW_LINE +
                    "\t         * server - enable server mode, also you should specify port" + NEW_LINE +
                                NEW_LINE +
                    "\t--port   set port (see also --client and --server switches)" + NEW_LINE +
                    "\t--ip     set server ip address (see also --client)" + NEW_LINE +
                    NEW_LINE +
            "USAGE EXAMPLES" + NEW_LINE +
                    NEW_LINE +
                    "\t[game run command] --mode single" + NEW_LINE +
                    "\t[game run command] --mode client --ip 43.145.2.232 --port 23235" + NEW_LINE +
                    "\t[game run command] --mode server --port 43562" + NEW_LINE +
            NEW_LINE;

    public HelpView() {
        logger.debug("creating help view");
        super.message = HELP_MESSAGE;
    }

    @Override
    protected void clearScreen() {
        // Dirty hack. Disable screen clearing.
    }
}
