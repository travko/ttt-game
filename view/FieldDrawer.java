package view;

import model.field.IField;
import org.apache.log4j.Logger;
import util.ClassName;

public class FieldDrawer {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static int getScreenFieldSize(final int fieldSize) {
        // field + border
        return fieldSize * 2 + 1;
    }

    private static char getFenceCharacter(final int row, final int column, final int screenFieldSize) {
        if (row % 2 == 0) {
            if (row == 0) {
                if (column == 0) return CharacterHelper.fenceLeftTopCorner;
                else if (column == screenFieldSize - 1) return CharacterHelper.fenceRightTopCorner;
                else return CharacterHelper.fenceHorizontal;
            } else if (row == screenFieldSize - 1) {
                if (column == 0) return CharacterHelper.fenceLeftBottomCorner;
                else if (column == screenFieldSize - 1) return CharacterHelper.fenceRightBottomCorner;
                else return CharacterHelper.fenceHorizontal;
            } else if (column % 2 == 0) {
                if (column == 0 || column == screenFieldSize - 1) return CharacterHelper.fenceVertical;
                else return CharacterHelper.fenceConnector;
            } else {
                return CharacterHelper.fenceHorizontal;
            }
        } else {
            return CharacterHelper.fenceVertical;
        }
    }

    private static void drawFenceLine(final int row, final int screenFieldSize) {
        for (int column = 0; column < screenFieldSize; column++) {
            System.out.print(getFenceCharacter(row, column, screenFieldSize));
            drawSpaceDelimiter();
        }
        System.out.println();
    }

    protected static void drawFieldLine(final int row, final int screenFieldSize, IField field) {
        for (int column = 0; column < screenFieldSize; column++) {
            if (column % 2 == 0) {
                System.out.print(getFenceCharacter(row, column, screenFieldSize));
            } else {
                System.out.print(CharacterHelper.GetCharacter(field.getCellState(row / 2, column / 2)));
            }
            drawSpaceDelimiter();
        }
        System.out.println();
    }

    public static void drawSpaceDelimiter() {
        System.out.print(CharacterHelper.spaceDelimiter);
    }

    public static void drawHorizontalScale(final int screenFieldSize) {
        drawSpaceDelimiter();
        drawSpaceDelimiter();
        for (int i = 0; i < screenFieldSize; i++) {
            if (i % 2 == 0) {
                if ((i != 0) && (i != screenFieldSize - 1)) {
                    System.out.print(CharacterHelper.fenceVertical);
                } else {
                    drawSpaceDelimiter();
                }
            } else {
                System.out.print(i / 2);
            }
            drawSpaceDelimiter();
        }
        System.out.println();
    }

    public static void drawVerticalScale(final int row, final int screenFieldSize) {
        if (row % 2 == 0) {
            if ((row != 0) && (row != screenFieldSize - 1)) {
                System.out.print(CharacterHelper.fenceHorizontal);
            } else {
                drawSpaceDelimiter();
            }
        } else {
            System.out.print(row/2);
        }
        drawSpaceDelimiter();

    }

    public static void drawField(IField field) {
        logger.debug("drawing game field " + field);

        if (field != null) {
            final int screenFieldSize = getScreenFieldSize(field.getFieldSize());
            drawHorizontalScale(screenFieldSize);
            for (int i = 0; i < screenFieldSize; i++) {
                drawVerticalScale(i, screenFieldSize);
                if (i % 2 == 0) {
                    drawFenceLine(i, screenFieldSize);
                } else {
                    drawFieldLine(i, screenFieldSize, field);
                }
            }
        }
    }
}