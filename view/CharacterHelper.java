package view;

import model.field.State;

public class CharacterHelper {
    public static final char crossCharacter = 'x';
    public static final char circleCharacter = 'o';
    public static final char emptyCharacter = ' ';
    public static final char fenceHorizontal = '-';
    public static final char fenceVertical = '|';
    public static final char fenceConnector = '+';
    public static final char fenceLeftTopCorner = '+';
    public static final char fenceRightTopCorner = '+';
    public static final char fenceLeftBottomCorner = fenceRightTopCorner;
    public static final char fenceRightBottomCorner = fenceLeftTopCorner;
    public static final char spaceDelimiter = ' ';

    public static char GetCharacter(State state) {
        switch (state) {
            case CIRCLE:
                return circleCharacter;
            case CROSS:
                return crossCharacter;
            default:
                return emptyCharacter;
        }
    }
}