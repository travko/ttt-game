package view;

import model.field.IField;
import org.apache.log4j.Logger;
import util.ClassName;

public class FieldView extends ConsoleView implements IFieldView {

    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private IField field;

    public void setField(IField field) {
        this.field = field;
    }

    public void drawField() {
        logger.debug("drawing field");
        if (field != null) {
            FieldDrawer.drawField(field);
        } else {
            logger.warn("field = null");
        }
    }

    @Override
    public void update() {
        logger.debug("updating console view");
        clearScreen();
        drawField();
        displayMessages();
    }
}
