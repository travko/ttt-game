package view;

import org.apache.log4j.Logger;
import util.ClassName;

public class ConsoleView {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    protected String message;
    protected String errorMessage;
    protected boolean showErrorMessage = false;
    protected final int LINES_PER_SCREEN = 50;

    protected void displayMessages() {
        logger.debug("displaying messages");
        if (showErrorMessage) {
            System.out.println(errorMessage);
            showErrorMessage = false;
        }
        if (message != null) {
            System.out.println(message);
        }
    }


    protected void clearScreen() {
        logger.debug("clearing screen");
        for (int i = 0; i < LINES_PER_SCREEN; i++) {
            System.out.println();
        }
    }

    public void update() {
        logger.debug("updating view");
        clearScreen();
        displayMessages();
    }

    public void setErrorMessage(final String errorMessage) {
        logger.debug("setting error message " + errorMessage);
        this.errorMessage = errorMessage;
        showErrorMessage = true;
    }

    public void setMessage(final String message) {
        logger.debug("setting message " + message);
        this.message = message;
    }

}
