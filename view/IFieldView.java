package view;

import model.field.IField;

public interface IFieldView {

    public void setField(IField field);
    public void update();
    public void setMessage(final String text);
    public void setErrorMessage(final String text);

}
