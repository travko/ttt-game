package model;

import model.field.Field;
import org.apache.log4j.Logger;
import util.ClassName;

import java.util.ArrayList;
import java.util.List;

public class GameModelHistory extends GameModel implements IModel {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private List<Field> fields = new ArrayList<>();

    public GameModelHistory(int fieldSize, int winLineLength) {
        super(fieldSize, winLineLength);
    }

    @Override
    public boolean doMove(int row, int column) {
        saveField();
        return super.doMove(row, column);
    }

    private void saveField() {
        logger.debug("saving field " + gameField);
        fields.add(gameField.getDeepCopy());
    }

    @Override
    public boolean restoreLastField() {
        logger.debug("restoring last field");
        return restoreField(fields.size() - 1);
    }

    @Override
    public boolean restoreField(int index) {
        int upBound = fields.size() - 1;
        if (index < 0 || index > upBound) {
            logger.debug(String.format("bad value of index %d, maximum value is %d", index, upBound));
            return false;
        } else {
            logger.debug("restoring field #" + index);
            logger.debug("field history\r\n" + getFieldHistory());
            gameField = fields.get(index);
            logger.debug(gameField);
            return true;
        }
    }

    private String getFieldHistory() {
        StringBuilder builder = new StringBuilder();
        for (Field field : fields) {
            builder.append(field.toString());
            builder.append("\r\n");
        }
        return builder.toString();
    }
}
