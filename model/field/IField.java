package model.field;

import model.field.State;

public interface IField {

    public boolean isCellEmpty(int row, int column);
    public State getCellState(int row, int column);
    public int getFieldSize();

}
