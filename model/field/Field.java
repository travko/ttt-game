package model.field;

import org.apache.log4j.Logger;
import util.ClassName;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.Arrays;

public class Field implements IField {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String SINGLE_SPACE = " ";
    private static final String EMPTY_STRING = "";

    private final State[][] field;
    private final int fieldSize;

    public Field(int fieldSize) {
        logger.debug("creating new field with size " + fieldSize );
        if (fieldSize < 1) {
            throw new IllegalArgumentException("fieldSize value can't be less then 1");
        }
        this.fieldSize = fieldSize;
        this.field = new State[fieldSize][fieldSize];
        clearFiled();
    }

    private Field(State[][] field, int fieldSize) {
        logger.debug("creating new field with field " + Arrays.deepToString(field));
        this.field = field;
        this.fieldSize = fieldSize;
    }

    private void clearRow(int rowIndex) {
        for (int i = 0; i < fieldSize; i++) {
            field[rowIndex][i] = State.NONE;
        }
    }

    public void clearFiled() {
        logger.debug("clearing field");
        for (int i = 0; i < fieldSize; i++) {
            clearRow(i);
        }
    }

    public void setCellState(int row, int column, State cellState) {
        logger.debug(String.format("setting new state %s to the cell [%d, %d]", cellState, row, column));
        if (isCellPositionCorrect(row, column) && isCellEmpty(row, column)) {
            field[row][column] = cellState;
        } else {
            throw new IllegalArgumentException();
        }

    }

    @Override
    public State getCellState(int row, int column) {
        if (!isCellPositionCorrect(row, column)) {
            throw new IllegalArgumentException();
        }
        return field[row][column];
    }


    @Override
    public boolean isCellEmpty(int row, int column) {
        if (!isCellPositionCorrect(row, column)) {
            throw new IllegalArgumentException();
        }
        return field[row][column] == State.NONE;
    }

    @Override
    public int getFieldSize() {
        return fieldSize;
    }

    public boolean isCellPositionCorrect(int row, int column) {
        logger.debug(String.format("check position [%d, %d] request ", row, column));
        return ((0 <= row) && (row < fieldSize)) &&
               ((0 <= column) && (column < fieldSize));
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (int i = 0; i < fieldSize; i++) {
            builder.append("[");
            for (int j = 0; j < fieldSize; j++) {
                builder.append(field[i][j]);
                builder.append( j < fieldSize - 1 ? SINGLE_SPACE : EMPTY_STRING);
            }
            builder.append("]");
        }
        builder.append("]");
        return builder.toString();
    }

    public Field getDeepCopy() {
        State[][] clonedField = new State[field.length][field[0].length];
        for (int i = 0; i < clonedField.length; i++) {
            System.arraycopy(field[i], 0, clonedField[i], 0, field[i].length);
        }

        return new Field(clonedField, fieldSize);
    }
}