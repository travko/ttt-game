package model;

import model.field.IField;
import org.apache.log4j.Logger;
import util.ClassName;

public class ModelProxy implements IModel {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private final IModel model;
    private ModelMoveEvent moveEvent;

    public interface ModelMoveEvent {
        public void modelMoveEvent(int row, int column);
    }

    public ModelProxy(IModel model) {
        if (model == null) {
            throw new IllegalArgumentException();
        }
        this.model = model;
    }

    public void setMoveEventCallback(ModelMoveEvent moveEvent) {
        if (moveEvent == null) {
            throw new IllegalArgumentException();
        }
        this.moveEvent = moveEvent;
    }

    @Override
    public boolean isGameFinished() {
        return model.isGameFinished();
    }

    @Override
    public String getGameWinner() {
        return model.getGameWinner();
    }

    @Override
    public boolean doMove(int row, int column) {
        logger.debug(String.format("doing handled move to [%d, %d]", row, column));
        boolean isMoveAccepted = doMoveNoHandle(row, column);
        if (isMoveAccepted && moveEvent != null) {
            logger.debug("calling move event handler");
            moveEvent.modelMoveEvent(row, column);
        }
        return isMoveAccepted;
    }

    public boolean doMoveNoHandle(int row, int column) {
        logger.debug(String.format("doing move to cell [%d, %d]", row, column));
        return model.doMove(row, column);
    }

    @Override
    public IField getField() {
        return model.getField();
    }

    @Override
    public boolean restoreField(int index) {
        return model.restoreField(index);
    }

    @Override
    public boolean restoreLastField() {
        return model.restoreLastField();
    }
}
