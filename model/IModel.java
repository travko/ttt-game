package model;

import model.field.IField;

public interface IModel {
    public boolean isGameFinished();
    public String getGameWinner();
    public boolean doMove(int row, int column);
    public IField getField();
    public boolean restoreField(int index);
    public boolean restoreLastField();
}
