package launch;

import org.apache.log4j.Logger;
import util.ClassName;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Parameters {

    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String GAME_MODE_SELECTOR = "--mode";
    private static final String IP_SELECTOR        = "--ip";
    private static final String PORT_SELECTOR      = "--port";

    private static final String FIELD_SIZE_SELECTOR   = "--size";
    private static final String WIN_LINE_LEN_SELECTOR = "--line";
    private static final String TURN_ORDER_SELECTOR   = "--order";

    private static final String GAME_MODE_SINGLE = "hotseat";
    private static final String GAME_MODE_SERVER = "server";
    private static final String GAME_MODE_CLIENT = "client";
    private static final String GAME_MODE_BOT    = "bot";

    private static final String IP_REGEXP = "(localhost)|((\\d++).(\\d++).(\\d++).(\\d++))";

    private static final String BAD_IP_MESSAGE = "Bad ip address: %s";
    private static final String BAD_PORT_MESSAGE = "Bad port: %s";
    private static final String UNKNOWN_GAME_MODE_MESSAGE = "Unknown game mode: %s";
    private static final String UNKNOWN_PARAMETER_MESSAGE = "Unknown parameter: %s";
    private static final String INCORRECT_FIELD_SIZE_MESSAGE = "Incorrect field size: %s";
    private static final String INCORRECT_WIN_LINE_LENGTH_MESSAGE = "Incorrect win line length: %s";

    private final List<Method> parameterParseMethods = new ArrayList<>();

    private GameMode gameMode = GameMode.HOTSEAT;
    private String   host     = "localhost";
    private int      port     = 45312;

    private int winLineLen  = 3;
    private int fieldSize   = 3;
    private int firstPlayer = 0;

    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.RUNTIME)
    private @interface ParameterParser {
        String parameterName();
    }

    public Parameters(String[] parameters) throws  ParameterException {
        logger.info("parsing cmdline parameters " + Arrays.toString(parameters));
        CreateAnnotatedMethodsList(ParameterParser.class);
        ParseParameters(parameters);
    }

    private void CreateAnnotatedMethodsList(Class<ParameterParser> annotation) {
        logger.debug("creating list of annotated methods");
        Method[] methods = this.getClass().getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(annotation)) {
                logger.debug("adding method " + method + " to list");
                parameterParseMethods.add(method);
            }
        }
        logger.debug("list of annotated methods crated, total entry count " + parameterParseMethods.size() );
    }

    private void ParseParameters(String[] parameters) throws ParameterException {
        for (int i = 0; i < parameters.length; i += 2) {
            String parameterName = parameters[i];
            String parameterValue = (i != parameters.length - 1) ? parameters[i + 1] : null;

            parseParameter(parameterName, parameterValue);
        }
    }

    private void parseParameter(String parameterName, String parameterValue) throws ParameterException {
        logger.debug("finding suitable parse method for " + parameterName + "@" + parameterValue);
        for (Method method : parameterParseMethods) {
            if (method.getAnnotation(ParameterParser.class).parameterName().equals(parameterName)) {
                logger.debug("invoking parse method " + method);
                invokeParsing(parameterValue, method);
                return;
            }
        }
        throw new ParameterException(String.format(UNKNOWN_PARAMETER_MESSAGE, parameterName));
    }

    private void invokeParsing(String parameterValue, Method method) throws ParameterException {
        try {
            method.setAccessible(true);
            method.invoke(this, parameterValue);
        } catch (IllegalAccessException e) {
            logger.warn("parsing method invoke exception: " + e.getMessage());
        } catch (InvocationTargetException e) {
            logger.warn("parsing exception " + e.getMessage() + "\r\n" + Arrays.toString(e.getStackTrace()));
            throw new ParameterException(e.getCause());
        }
    }


    @ParameterParser(parameterName = GAME_MODE_SELECTOR)
    private void parseGameMode(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        switch (value) {
            case GAME_MODE_SINGLE: this.gameMode = GameMode.HOTSEAT; break;
            case GAME_MODE_SERVER: this.gameMode = GameMode.SERVER;  break;
            case GAME_MODE_CLIENT: this.gameMode = GameMode.CLIENT;  break;
            case GAME_MODE_BOT:    this.gameMode = GameMode.BOT;     break;
            default: throw new ParameterException(String.format(UNKNOWN_GAME_MODE_MESSAGE, value));
        }
    }

    @ParameterParser(parameterName = IP_SELECTOR)
    private void parseIp(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        Matcher matcher = Pattern.compile(IP_REGEXP).matcher(value);
        if (matcher.matches()) {
            this.host = value;
        } else {
            throw new ParameterException(String.format(BAD_IP_MESSAGE, value));
        }
    }

    @ParameterParser(parameterName = PORT_SELECTOR)
    private void parsePort(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        try {
            port = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ParameterException(String.format(BAD_PORT_MESSAGE, value));
        }
    }

    @ParameterParser(parameterName = FIELD_SIZE_SELECTOR)
    private void parseFieldSize(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        try {
            fieldSize = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ParameterException(String.format(INCORRECT_FIELD_SIZE_MESSAGE, value));
        }
    }

    @ParameterParser(parameterName = WIN_LINE_LEN_SELECTOR)
    private void parseWinLineLength(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        try {
            winLineLen = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ParameterException(String.format(INCORRECT_WIN_LINE_LENGTH_MESSAGE, value));
        }
    }

    @ParameterParser(parameterName = TURN_ORDER_SELECTOR)
    private void parseTurnOrder(String value) throws ParameterException {
        logger.debug("try to parse " + value);
        this.firstPlayer = 0;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public int getWinLineLen() {
        return winLineLen;
    }

    public int getFieldSize() {
        return fieldSize;
    }

    public int getFirstPlayer() {
        return firstPlayer;
    }
}
