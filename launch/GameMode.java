package launch;

public enum GameMode {
    HOTSEAT,
    SERVER,
    CLIENT,
    BOT
}
