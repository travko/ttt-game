package launch;

public class ParameterException extends Exception {

    ParameterException(String message) {
        super(message);
    }

    ParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    ParameterException(Throwable cause) {
        super(cause);
    }
}
