import launch.GameLauncher;
import launch.Parameters;
import org.apache.log4j.Logger;
import util.ClassName;
import view.HelpView;

import java.util.Arrays;

public class Main {

    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    public static void startProgram(String[] params) {
        try {
            GameLauncher.InitGame(new Parameters(params)).beginGame();
        } catch (Exception e) {
            logger.error(e.toString(), e);
        }
    }

    public static void main(String[] args)  {
        logger.info("ttt-game started");
        if (args.length == 0) {
            logger.info("args.length == 0 => showing help view");
            new HelpView().update();
        } else {
            logger.info("starting program");
            startProgram(args);
        }
        logger.info("ttt-game finished");
    }
}

