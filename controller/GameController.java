package controller;

import action.*;
import model.IModel;
import org.apache.log4j.Logger;
import util.ClassName;
import util.Tuple;
import view.IFieldView;

import java.io.IOException;

public class GameController {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String BAD_INPUT_MESSAGE = "Wrong command";
    private static final String WRONG_MOVE_MESSAGE = "Wrong move";
    private static final String THE_WINNER_IS_MESSAGE = "The winner is %s";
    private static final String PLAYER_TURN_MESSAGE = "Player #%d turn (you are #%d)";
    private static final String CAN_NOT_SET_HISTORY_MESSAGE = "Can't set history";

    private final IModel model;
    private final IFieldView fieldView;
    private final PlayersController players;

    private boolean isGameFinished = false;

    public GameController(IModel model, IFieldView fieldView, PlayersController playersController)  {
        logger.debug("creating new game controller");
        this.model = model;
        this.fieldView = fieldView;
        this.players = playersController;
        logger.debug("mapping players actions to functions");
        players.setActionDelegate(UserAction.QUIT, new PlayersController.Delegate() {
            @Override
            public void callDelegate(Object object) {
                quitAction();
            }
        });
        players.setActionDelegate(UserAction.GAME_MOVE, new PlayersController.Delegate() {
            @Override
            public void callDelegate(Object object) {
                gameMoveAction(object);
            }
        });
        players.setActionDelegate(UserAction.BAD_INPUT, new PlayersController.Delegate() {
            @Override
            public void callDelegate(Object object) {
                badInputAction();
            }
        });
        players.setActionDelegate(UserAction.SET_HISTORY, new PlayersController.Delegate() {
            @Override
            public void callDelegate(Object object) {
                setHistory(object);
            }
        });

        updateView();
        logger.debug("creating complete");
    }

    private void quitAction() {
        logger.debug("quit action occurred");
        isGameFinished = true;
    }

    private void badInputAction() {
        logger.debug("bad input action occurred");
        setViewErrorMessage(BAD_INPUT_MESSAGE);
    }

    private void gameMoveAction(Object object) {
        logger.debug("game move action occurred");
        Tuple<?> position = (Tuple)object;
        final int row = (Integer)position.getValue(0);
        final int column = (Integer)position.getValue(1);

        doMove(row, column);
    }

    private void doMove(int row, int column) {
        logger.debug("doing game move");
        if (model.doMove(row, column)) {
            if (model.isGameFinished()) {
                logger.info("last move occurred, finishing move");
                setViewMessage(String.format(THE_WINNER_IS_MESSAGE, model.getGameWinner()));
                isGameFinished = true;
            } else {
                switchNextPlayer();
            }
        } else {
            logger.warn("wrong move");
            setViewErrorMessage(WRONG_MOVE_MESSAGE);
        }
    }

    private void setHistory(Object object) {
        logger.debug("restoring field #" + object);
        if (model.restoreField((Integer)object)) {
            logger.debug("field restoring successful");
        } else {
            setViewErrorMessage(CAN_NOT_SET_HISTORY_MESSAGE);
            logger.debug("field restoring field");
        }
    }

    private void switchNextPlayer() {
        logger.debug("switching to the next player");
        players.switchNextPlayer();
    }

    private void setPlayerTurnMessage() {
        if (!isGameFinished) {
            setViewMessage(String.format(PLAYER_TURN_MESSAGE,
                    players.getCurrentPlayer() + 1,
                    players.getCurrentLocalPlayer() + 1));
        }
    }

    private void setViewErrorMessage(String message) {
        if (fieldView != null) {
            fieldView.setErrorMessage(message);
        }
    }

    private void setViewMessage(String message) {
        if (fieldView != null) {
            fieldView.setMessage(message);
        }
    }

    private void updateView() {
        logger.debug("updating view");
        if (fieldView != null) {
            logger.debug("setting field to view");
            fieldView.setField(model.getField());
            logger.debug("setting view message");
            setPlayerTurnMessage();
            fieldView.update();
        } else {
            logger.warn("view is not set");
        }
    }

    public void beginGame() throws IOException {
        logger.info("beginning game");
        while (!isGameFinished) {
            players.waitPlayerAction();
            updateView();
        }
        logger.info("game is finished");
    }
}
