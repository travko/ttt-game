package streams;

public interface IStreamCommutator {

    public void writeLineRemote(String line);
    public void writeLineBroadcast(String line, int broadcastSource);
    public int getIOStreamsCount();
    public String readLine(int pairIndex);
    public boolean isRemoteIOStream(int pairIndex);
}
