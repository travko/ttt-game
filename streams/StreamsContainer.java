package streams;

import org.apache.log4j.Logger;
import util.ClassName;

import java.io.*;

public class StreamsContainer {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private final PrintStream printStream;
    private final boolean isRemote;
    private final BufferedReader streamReader;

    public StreamsContainer(OutputStream outputStream, InputStream inputStream, boolean isRemote) {
        logger.debug("creating new container " + this);
        this.printStream = new PrintStream(outputStream, true);
        this.streamReader = new BufferedReader(new InputStreamReader(inputStream));
        this.isRemote = isRemote;
    }

    public PrintStream getPrintStream() {
        return printStream;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public BufferedReader getReader() {
        return streamReader;
    }
}
