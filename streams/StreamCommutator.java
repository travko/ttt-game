package streams;

import org.apache.log4j.Logger;
import util.ClassName;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class StreamCommutator implements IStreamCommutator {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    public static final boolean REMOTE = true;
    public static final boolean LOCAL = false;

    private List<StreamsContainer> streamsList = new ArrayList<>();

    public void addStreams(OutputStream outputStream, InputStream inputStream, boolean isRemote) {
        logger.debug("adding new streams");
        addStreams(new StreamsContainer(outputStream, inputStream, isRemote));
    }


    public void addStreams(StreamsContainer container) {
        logger.debug("adding streams container " + container);
        streamsList.add(container);
    }

    public StreamCommutator reverseOrder() {
        logger.debug("reversing order of streams pairs");
        StreamCommutator newCommutator = new StreamCommutator();

        for (int i = streamsList.size() - 1; i >= 0; i--) {
            newCommutator.addStreams(streamsList.get(i));
        }

        return newCommutator;
    }

    public void removeStreamPair(int streamPairIndex) {
        logger.debug("removing streams for index " + streamPairIndex);
        assert(streamPairIndex < streamsList.size());
        streamsList.remove(streamPairIndex);
    }

    @Override
    public void writeLineBroadcast(String line, int broadcastSource) {
        logger.debug("writing line " + line + " from index " + broadcastSource);
        if (!streamsList.get(broadcastSource).isRemote()) {
            writeLineRemote(line);
        }
    }

    @Override
    public void writeLineRemote(String line) {
        logger.debug("sending remote: " + line);
        for (StreamsContainer container : streamsList) {
            logger.debug("trying to send to " + container);
            writeLineIfRemote(line, container);
        }
    }

    private void writeLineIfRemote(String line, StreamsContainer container) {
        if (container.isRemote()) {
            logger.debug("writing line " + line + " to " + container);
            container.getPrintStream().println(line);
        }
    }

    @Override
    public int getIOStreamsCount() {
        return streamsList.size();
    }

    @Override
    public String readLine(int pairIndex) {
        String result;
        logger.debug("reading line from streamsList[" + pairIndex + "]");
        try {
            result = streamsList.get(pairIndex).getReader().readLine();
        } catch (IOException e) {
            result = null;
        }
        logger.debug("have read " + result);
        return result;
    }

    @Override
    public boolean isRemoteIOStream(int pairIndex) {
        return streamsList.get(pairIndex).isRemote();
    }
}
