package action;

import action.reader.IDataReader;
import org.apache.log4j.Logger;
import util.ClassName;

import java.util.*;

public class UserActionProvider implements IUserActionProvider {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private final Map<IDataReader<?>, UserAction> dataReaderMap = new HashMap<>();
    private IDataReader lastAcceptedDataReader;

    public void addDataReader(final IDataReader<?> dataReader, UserAction action) {
        logger.info("adding data reader " + dataReader + " for action " + action);
        if (dataReader != null) {
            dataReaderMap.put(dataReader, action);
        } else {
            logger.warn("dataReader value is null");
        }
    }

    @Override
    public Object getLastUserActionData() {
        logger.debug("returning last user action data");
        if (lastAcceptedDataReader != null) {
            Object obj = lastAcceptedDataReader.getData();
            logger.debug("last user action data is " + obj);
            return obj;
        } else {
            return null;
        }
    }

    private boolean isDataReaderSuitable(final String inputLine, final IDataReader<?> reader) {
        logger.debug("checking reader " + reader + " for line " + inputLine);
        reader.setDataString(inputLine);
        if (reader.isDataMatch()) {
            logger.debug("reader found");
            lastAcceptedDataReader = reader;
            return true;
        }
        logger.debug("mismatch reader" + reader + " for line " + inputLine);
        return false;
    }

    @Override
    public UserAction getUserAction(final String inputLine) {
        logger.debug("looking for suitable reader for string " + inputLine);
        for (Map.Entry<IDataReader<?>, UserAction> entry : dataReaderMap.entrySet()) {
            if (isDataReaderSuitable(inputLine, entry.getKey())) {
                lastAcceptedDataReader = entry.getKey();
                logger.debug("user action is " + entry.getValue());
                return entry.getValue();
            }
        }
        logger.warn("suitable reader didn't find, unknown user action");

        lastAcceptedDataReader = null;
        return UserAction.BAD_INPUT;
    }
}
