package action;

public enum UserAction {
    QUIT,
    GAME_MOVE,
    SET_HISTORY,
    BAD_INPUT
}
