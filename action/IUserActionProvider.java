package action;

public interface IUserActionProvider {
    public UserAction getUserAction(final String inputLine);
    public Object getLastUserActionData();
    //public void addDataReader(final IDataReader<? extends Object> dataReader, UserAction action);
}
