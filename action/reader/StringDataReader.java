package action.reader;

import org.apache.log4j.Logger;
import util.ClassName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringDataReader implements IDataReader<String> {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String CMD = "command";
    private static final String QUIT_REGEX = "^\\s*+(?<" + CMD + ">%s)\\s*+$";
    private final Pattern pattern;

    Matcher dataMatcher;

    public StringDataReader(final String string) {
        logger.debug("crating data reader for string " + string);
        pattern = Pattern.compile(String.format(QUIT_REGEX, string));
    }

    @Override
    public void setDataString(String dataString) {
        logger.debug("trying to apply data reader for: " + dataString);
        dataMatcher = pattern.matcher(dataString);
    }

    @Override
    public boolean isDataMatch() {
        return dataMatcher != null && dataMatcher.matches();
    }

    @Override
    public String getData() {
        if (isDataMatch()) {
            String extractedString = dataMatcher.group(CMD);
            logger.debug("extracted data is " + extractedString);
            return extractedString;
        } else {
            return null;
        }
    }

}