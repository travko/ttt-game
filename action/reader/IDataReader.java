package action.reader;

public interface IDataReader<T> {
    public void setDataString(final String dataString);
    public boolean isDataMatch();
    public T getData();
}
