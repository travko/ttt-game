package action.reader;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import action.reader.IDataReader;
import org.apache.log4j.Logger;
import util.ClassName;
import util.Tuple;

public class MoveDataReader implements IDataReader<Tuple<Integer>> {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String GROUP_ROW = "row";
    private static final String GROUP_COLUMN = "column";
    private static final String TOW_NUMBERS_REGEX_FORMAT = "^\\s*+(?<%s>\\d++)\\s++(?<%s>\\d++)\\s*+$";
    private static final String TOW_NUMBERS_REGEX = String.format(TOW_NUMBERS_REGEX_FORMAT, GROUP_ROW, GROUP_COLUMN);
    private static final Pattern pattern = Pattern.compile(TOW_NUMBERS_REGEX);

    private static final String MOVE_ENCODING_PATTERN = "%d %d";
    Matcher dataMatcher;


    public void setDataString(final String dataString) {
        logger.debug("tyring to apply reader for: " + dataString);
        dataMatcher = pattern.matcher(dataString);
    }

    public boolean isDataMatch() {
        return dataMatcher != null && dataMatcher.matches();
    }

    public Tuple<Integer> getData() {
        if (isDataMatch()) {
            final int row = Integer.valueOf(dataMatcher.group(GROUP_ROW));
            final int column = Integer.valueOf(dataMatcher.group(GROUP_COLUMN));
            logger.debug("extracted data: " + row + " and " + column);
            return new Tuple<>(new Integer[] {row, column});
        } else {
            return null;
        }
    }

    public static String EncodeMove(Tuple<Integer> move) {
        logger.debug("encoding message for " + move );
        if (move.length() != 2) {
            throw new IllegalArgumentException("incorrect move data " + move);
        }
        return String.format(MOVE_ENCODING_PATTERN, move.getValue(0), move.getValue(1));
    }
}
