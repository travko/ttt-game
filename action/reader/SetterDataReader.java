package action.reader;

import org.apache.log4j.Logger;
import util.ClassName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SetterDataReader implements IDataReader<Integer> {
    private static final Logger logger = Logger.getLogger(ClassName.getCurrentClassName());

    private static final String DATA = "data";
    private static final String QUIT_REGEX = "^\\s*+(%s)\\s++(?<" + DATA + ">\\d++)\\s*+$";

    private final Pattern pattern;
    Matcher dataMatcher;

    public SetterDataReader(String command) {
        if (command.length() == 0) {
            throw new IllegalArgumentException();
        }

        pattern = Pattern.compile(String.format(QUIT_REGEX, command));
    }

    @Override
    public void setDataString(String dataString) {
        logger.debug("tyring to apply reader for: " + dataString);
        dataMatcher = pattern.matcher(dataString);
    }

    @Override
    public boolean isDataMatch() {
        return dataMatcher != null && dataMatcher.matches();
    }

    @Override
    public Integer getData() {
        if (isDataMatch()) {
            String extractedString = dataMatcher.group(DATA);
            logger.debug("extracted data is " + extractedString);
            return Integer.parseInt(extractedString);
        } else {
            return null;
        }
    }
}
